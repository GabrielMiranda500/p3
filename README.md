[![Open in Visual Studio Code](https://classroom.github.com/assets/open-in-vscode-c66648af7eb3fe8bc4f294546bfd86ef473780cde1dea487d3c4ff354943c9ae.svg)](https://classroom.github.com/online_ide?assignment_repo_id=8221749&assignment_repo_type=AssignmentRepo)
# Hello-To-Github-Classroom
Olá, bem-vindo a sala do Github Classroom!

Você entrou em uma sala de aula do GitHub, o qual clona um repositório individualmente para cada aluno e o professor consegue avaliar o código disponível nele!.

Muito bem, agora que você entrou é hora de LINKAR SEU GITHUB COM O PERFIL GOOGLE CLASSROOM! Para que o Github saiba quem é você na sala do Google Classroom. Não se preocupe, a lista de nomes de alunos já foi importada, você apenas precisa indicar para o github qual o seu nome. Se você o já fez, ótimo.

Você deve linkar para concluir esta atividade.

Linkou? Agora é hora de tirar um print da tela deste repositório, voltar à atividade no google classroom, anexar o print e entregar a atividade :)
